const copyClipboard = (e, text) => {
  e.preventDefault();
  if (navigator.clipboard && text) {
    navigator.clipboard.writeText(text);
  } else {
    console.info("navigator.clipboard not available or text empty", {
      "navigator.clipboard": navigator.clipboard,
      text,
    });
  }
};

const App = () => (
  <div className="wrapper">
    <div className="content">
      <div className="title">Nosso Pix:</div>
      <div className="pix">451.995.628-51</div>
      <button onClick={(e) => copyClipboard(e, "45199562851")}>COPIAR</button>
    </div>
  </div>
);
export default App;
